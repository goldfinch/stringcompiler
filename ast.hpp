#pragma once

#include <ostream>
#include <string>

namespace ast
{
    struct Node
    {
        virtual void print(std::ostream& o) const = 0;
        virtual double calc() const = 0;
        virtual ~Node() = default;
    };

    struct NumLiteral : Node
    {
        double value;

        NumLiteral(double value) : value(value) {}

        virtual void print(std::ostream& o) const override { o << value; }
        virtual double calc() const override { return value; }
    };

    struct FnCall : Node
    {
        std::string f;
        Node* expr;

        FnCall(std::string f, Node* expr) : f(f), expr(expr) {}

        virtual void print(std::ostream& o) const override
        {
            o << f << '(';
            expr->print(o);
            o << ')';
        }

        virtual double calc() const override
        {
            auto E = expr->calc();
            if (f == "abs")
                return abs(E);

            throw std::runtime_error("unknown function");
        }
    };

    struct UnaryExpression : Node
    {
        enum Op
        {
            Neg,
            Compl,
        };

        Op op;
        Node* expr;

        UnaryExpression(Op op, Node* expr) : op(op), expr(expr) {}

        virtual void print(std::ostream& o) const override
        {
            o << '(';
            switch (op)
            {
            case Neg: o << '-'; break;
            case Compl: o << '~'; break;
            default:
                throw std::runtime_error("unknown unary op");
            }
            expr->print(o);
            o << ')';
        }

        virtual double calc() const override
        {
            auto E = expr->calc();
            switch (op)
            {
            case Neg: return -E;
            case Compl: return ~int(E);
            default:
                throw std::runtime_error("unknown unary op");
            }
        }
    };

    struct BinaryExpression : Node
    {
        enum Op
        {
            Mul, Div,
            Add, Sub,
            Shr, Shl,
            Or, And,
        };

        Op op;
        Node* lhs;
        Node* rhs;

        BinaryExpression(Op op, Node* lhs, Node* rhs) : op(op), lhs(lhs), rhs(rhs) {}

        virtual void print(std::ostream& o) const override
        {
            o << '(';
            lhs->print(o);
            switch (op)
            {
            case Add: o << '+'; break;
            case Sub: o << '-'; break;
            case Mul: o << '*'; break;
            case Div: o << '/'; break;
            case Shl: o << "<<"; break;
            case Shr: o << ">>"; break;
            case Or: o << "|"; break;
            case And: o << "&"; break;
            default:
                throw std::runtime_error("unknown binary op");
            }
            rhs->print(o);
            o << ')';
        }

        virtual double calc() const override
        {
            auto L = lhs->calc();
            auto R = rhs->calc();
            switch (op)
            {
            case Add: return L + R;
            case Sub: return L - R;
            case Mul: return L * R;
            case Div: return L / R;
            case Shl: return int(L) << int(R);
            case Shr: return int(L) >> int(R);
            case Or: return int(L) | int(R);
            case And: return int(L) & int(R);
            default:
                throw std::runtime_error("unknown binary op");
            }
        }
    };
}