#pragma once

#include "ast.hpp"
#include "gc.hpp"

namespace parsing
{
    inline bool is_digit(char c)
    {
        return c >= '0' && c <= '9';
    }

    inline double parse_num(const char*& str)
    {
        double num = 0;

        for (; is_digit(*str); ++str)
        {
            num = num * 10 + (*str - '0');
        }

        return num;
    }

    inline bool test(const char*& str, const char* expected)
    {
        auto expectedLen = std::strlen(expected);
        if (std::strncmp(str, expected, expectedLen) != 0)
            return false;

        str += expectedLen;
        return true;
    }

    inline bool check_un_op(const char*& str, ast::UnaryExpression::Op& op)
    {
        if (test(str, "~"))
        {
            op = ast::UnaryExpression::Compl;
            return true;
        }
        
        if (test(str, "-"))
        {
            op = ast::UnaryExpression::Neg;
            return true;
        }

        return false;
    }

    struct BinOpTableEntry
    {
        ast::BinaryExpression::Op op;
        const char* token;
        int priority;

        bool invalid() const { return !token; }
    };

    inline BinOpTableEntry check_bin_op(const char*& str)
    {
        static BinOpTableEntry table[] =
        {
            {ast::BinaryExpression::Op::Or , "|", 3},
            {ast::BinaryExpression::Op::And, "&", 4},
            {ast::BinaryExpression::Op::Shr, ">>", 5},
            {ast::BinaryExpression::Op::Shl, "<<", 5},
            {ast::BinaryExpression::Op::Add, "+", 8},
            {ast::BinaryExpression::Op::Sub, "-", 8},
            {ast::BinaryExpression::Op::Mul, "*", 9},
            {ast::BinaryExpression::Op::Div, "/", 9},
            {ast::BinaryExpression::Op::Div, "%", 9},
        };

        for (auto&& e : table)
        {
            if (test(str, e.token))
                return e;
        }

        BinOpTableEntry invalidEntry;
        invalidEntry.token = nullptr;
        return invalidEntry;
    }

    class Parser
    {
    public:
        explicit Parser(const char* str) : str(str) {}

        ast::Node* parse()
        {
            BinOpTableEntry dummyOp;
            return parse_binary_expr(0, dummyOp);
        }

    private:
        void expect_closing_paren()
        {
            if (*str++ != ')')
                throw std::runtime_error("expected `)`");
        }

        ast::Node* parse_simple_expr()
        {
            if (is_digit(*str))
                return gc.alloc<ast::NumLiteral>(parse_num(str));

            if (test(str, "abs("))
            {
                auto arg = parse();
                expect_closing_paren();
                return gc.alloc<ast::FnCall>("abs", arg);
            }

            if (*str == '(')
            {
                ++str;
                auto node = parse();
                expect_closing_paren();
                return node;
            }

            throw std::runtime_error("unexpected token");
        }

        ast::Node* parse_unary_expr()
        {
            ast::UnaryExpression::Op unOp;
            if (check_un_op(str, unOp))
            {
                return gc.alloc<ast::UnaryExpression>(unOp, parse_simple_expr());
            }

            return parse_simple_expr();
        }

        ast::Node* parse_binary_expr(int minPriority, BinOpTableEntry& op)
        {
            auto lhs = parse_unary_expr();

            op = check_bin_op(str);
            if (op.invalid())
                return lhs;

            while (op.priority > minPriority)
            {

                BinOpTableEntry nextOp;
                auto rhs = parse_binary_expr(op.priority, nextOp);
                lhs = gc.alloc<ast::BinaryExpression>(op.op, lhs, rhs);
                op = nextOp;
            }

            return lhs;
        }

    private:
        GC gc;
        const char* str;
    };
}