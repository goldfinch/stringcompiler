#pragma once

#include "ast.hpp"

#include <deque>
#include <memory>

class GC
{
public:
    GC() {}

    template<typename T, typename... A>
    T* alloc(A... a)
    {
        m_nodes.emplace_back(std::make_unique<T>(a...));
        return static_cast<T*>(m_nodes.back().get());
    }

private:
    std::deque<std::unique_ptr<ast::Node>> m_nodes;
};