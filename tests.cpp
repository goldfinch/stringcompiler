#include "ast.hpp"
#include "parse.hpp"

#define CATCH_CONFIG_CPP11_NULLPTR
#define CATCH_CONFIG_MAIN
#include "catch.hpp"

static std::string parse(const char* str)
{
    parsing::Parser parser(str);
    auto node = parser.parse();
    REQUIRE(node != nullptr);

    std::stringstream ss;
    node->print(ss);
    return ss.str();
}

TEST_CASE("parse numbers (impl)")
{
    auto parse_num = [](const char* str)
    {
        auto num = parsing::parse_num(str);
        REQUIRE(*str == '\0');
        return num;
    };

    REQUIRE(parse_num("1") == 1);
    REQUIRE(parse_num("0") == 0);
    REQUIRE(parse_num("1234567890") == 1234567890);
}

TEST_CASE("parse numbers")
{
    REQUIRE(parse("1") == "1");
    REQUIRE(parse("12") == "12");
    REQUIRE(parse("0") == "0");
}

TEST_CASE("parse parentheses")
{
    REQUIRE(parse("(1)") == "1");
}

TEST_CASE("parse unary expressions")
{
    REQUIRE(parse("-1") == "(-1)");
    REQUIRE(parse("~1") == "(~1)");

    REQUIRE(parse("-(1)") == "(-1)");
}

TEST_CASE("parse binary expressions")
{
    REQUIRE(parse("1+2") == "(1+2)");
    REQUIRE(parse("1*2") == "(1*2)");
    REQUIRE(parse("1-2") == "(1-2)");
    REQUIRE(parse("1/2") == "(1/2)");
    REQUIRE(parse("1+2+3") == "((1+2)+3)");
    REQUIRE(parse("1+2*3") == "(1+(2*3))");
    REQUIRE(parse("(1+2)*3") == "((1+2)*3)");
    REQUIRE(parse("(1+2)*(3+4)") == "((1+2)*(3+4))");
    REQUIRE(parse("1*2+3*4") == "((1*2)+(3*4))");
}

TEST_CASE("parse calls")
{
    REQUIRE(parse("abs(-1)") == "abs((-1))");
}

TEST_CASE("parse complex expressions")
{
    REQUIRE(parse("-1-2") == "((-1)-2)");
    REQUIRE(parse("(-1)-2") == "((-1)-2)");
    REQUIRE(parse("-(1-2)") == "(-(1-2))");
}

static double eval(const char* str)
{
    parsing::Parser parser(str);
    auto node = parser.parse();
    REQUIRE(node != nullptr);

    return node->calc();
}

TEST_CASE("evaluate")
{
    REQUIRE(eval("1") == 1);
    REQUIRE(eval("12") == 12);

    REQUIRE(eval("1+20") == 21);
    REQUIRE(eval("12*2") == 24);
    REQUIRE(eval("12/2") == 6);
    REQUIRE(eval("12-2") == 10);
    REQUIRE(eval("1<<2") == 4);
    REQUIRE(eval("4>>2") == 1);
    REQUIRE(eval("3&2") == 2);
    REQUIRE(eval("1|2") == 3);

    REQUIRE(eval("1+20+300") == 321);
    REQUIRE(eval("1+20*2") == 41);
    REQUIRE(eval("(1+20)*2") == 42);

    REQUIRE(eval("abs(-1)") == 1);

    REQUIRE(eval("120*4+(123/2+3)+((1+2)/(2*6))+(3+4*1)") == 551.75);
}